# Weekli Meal

## Qu'est ce que Weekli Meal ?

Weekli Meal c'est une application qui permet de faire un menu de recette pour la semaine en fonction des recettes ajoutées en favoris. Il sera également possible de générer une liste de course à partir des ingrédients des recettes choisies.

## Environnement technique

BDD : MySQL
Front-end : React Js
Back-end : Node.Js Express

## Avancée du projet 

Modèle de base de données fait. \
Structure de la BDD MySQL fait. \
Initialisation du Front avec React et Vite. \
React Router DOM + Outlet \
Création des pages et des routes vers Home, Menus, Favoris, Listes, Profil. 

#### Composants : 
Header \
Hero \
Title \
Tags \
Add button \
Recipe-Card \
Bloc Filtres \

#### Fonctionalités :
Like/Dislike avec React State \
Filtres sur "Régimes alimentaires" avec .filter et .map \



## À faire 

Front-end :

- Search Bar
- Type-Card