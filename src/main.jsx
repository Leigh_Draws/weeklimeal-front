import React from "react";
import "./index.css";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, } from "react-router-dom";
import Home from "./pages/Home";
import MainLayout from "./layout/MainLayout";
import FavoritesPage from "./pages/Favoris";
import MenusPage from "./pages/Menus";
import ListesPage from "./pages/Lists";
import ProfilePage from "./pages/Profile";

const router = createBrowserRouter(
  
  [
    {
      path: "/", //page d'accueil
      element: <MainLayout/>,
      children: [
        //lien vers Home 
         {
          path: "/",
          element: <Home/>,
         },
         {
          path: "/favoris",
          element: <FavoritesPage />,
         },
         {
          path: "/menus",
          element: <MenusPage />,
         },
         {
          path: "/lists",
          element: <ListesPage />,
         },
         {
          path: "/profile",
          element: <ProfilePage />,
         },


      ]

    }
  ]
)


ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>
);
