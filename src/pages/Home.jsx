import Hero from "../components/Hero";
import RecipesList from "../components/RecipesList";
import { useOutletContext } from "react-router-dom";
import Title from "../components/Title";
import BlocFilters from "../components/BlocFilters";
import { useState } from "react";

function Home() {
  const { recipeData } = useOutletContext();

  const [recipes, setRecipes] = useState(recipeData);

  function filterRecipes(diet) {
    const recipesFiltered = recipeData.filter((eachRecipe) =>
      eachRecipe.diet.includes(diet)
    );
    setRecipes(recipesFiltered);
    console.log(recipesFiltered)
  }


  return (
    <>
      <Hero />
      <section>{/* Type de cuisine */}</section>
      <section>
        <Title title="Toutes les recettes" />
        <BlocFilters filterRecipes={filterRecipes} setRecipes={setRecipes} allRecipes={recipeData}/>
        <RecipesList allRecipes={recipes} />
      </section>
    </>
  );
}

export default Home;
