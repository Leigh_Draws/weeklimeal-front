import { Outlet } from "react-router-dom";
import Header from "../components/Header";
import '../index.css'
import recipesData from "../data/recipes";

function MainLayout() {

    const recipeData = recipesData

    return (
        <>
        <Header />
        <main>
            <Outlet context={{recipeData}}/>
        </main>
        <footer>
            <p>Copyright ©2024 - Clavequin Yaël</p>
        </footer>
        </>
    )

}

export default MainLayout