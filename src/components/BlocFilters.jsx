/* eslint-disable react/prop-types */
import "../components/BlocFilters.css";
import SelectDiet from "./SelectDiet";
import CloseIcon from "@mui/icons-material/Close";
import SelectType from "./SelectType";

function BlocFilters({ filterRecipes, setRecipes, allRecipes }) {
  return (
    <>
      <div className="bloc-filters">
        <div className="button-filters">
          <h4>FILTRES</h4>
          <button
            className="close-button"
            title="Supprimer les filtres"
            onClick={() => setRecipes(allRecipes)}
          >
            <CloseIcon />
          </button>
        </div>
        <div className="all-filters">
          <div className="select-box-diet">
            <SelectDiet filterRecipes={filterRecipes} setRecipes={setRecipes} />
          </div>
          <div className="select-box-type">
            <SelectType />
          </div>
        </div>
      </div>
    </>
  );
}

export default BlocFilters;
