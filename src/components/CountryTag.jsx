/* eslint-disable react/prop-types */
import "../components/Tag.css"

function CountryTag(props) {

    const { country } = props
    
    return(
        <>
        <h4 className="tag">{ country }</h4>
        </>
    )
}

export default CountryTag