/* eslint-disable react/prop-types */
import "../components/Tag.css"

function Tag(props) {

    const { tags } = props
    return(
        <>
        <h4 className="tag">{ tags }</h4>
        </>
    )
}

export default Tag