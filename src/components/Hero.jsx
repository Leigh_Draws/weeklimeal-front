import Shakshuka from "../assets/images/popular-1.png";
import "./Hero.css";

function Hero() {
  return (
    <>
      <div className="hero">
        <div className="hero-text">
          <div className="hero-titles">
            {/* Titre ne change pas */}
            <h1 className="hero-popular">Les Populaires</h1>
            {/* Une fois les données récupérées, les faire passer ici */}
            <h2 className="hand-writing">Spaghetti Shakshuka</h2>
          </div>
          <p className="hero-duration">25 min</p>
        </div>
        <div className="hero-img">
          <div className="hero-filter"></div>
          <img src={Shakshuka} alt="Spaghetti Shakshuka" />
        </div>
      </div>
    </>
  );
}

export default Hero;
