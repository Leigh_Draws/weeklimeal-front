/* eslint-disable react/prop-types */

import RecipeCard from "./RecipeCard";
import "../components/RecipeList.css";

function RecipesList(props) {
  const { allRecipes } = props;

  return (
    <>
      <div className="all-recipe-grid">
        {allRecipes.length === 0 ? (
          <h3 className="not-found">Pas de recettes trouvées</h3>
        ) : (
          allRecipes.map((eachrecipe) => (
            <RecipeCard
              key={eachrecipe.recipe_id}
              id={eachrecipe.recipe_id}
              name={eachrecipe.name}
              duration={eachrecipe.preparation_duration}
              country={eachrecipe.country_name}
              tags={eachrecipe.tags}
              image={eachrecipe.image_link}
            />
          ))
        )}
      </div>
    </>
  );
}

export default RecipesList;
