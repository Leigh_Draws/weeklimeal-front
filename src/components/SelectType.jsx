/* eslint-disable react/prop-types */
import { Box, FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useState } from "react";
import "../components/SelectDiet.css";

function SelectType() {
  const [type, setType] = useState("");

    const handleChange = (event) => {
    const selectedDiet = event.target.value
    setType(selectedDiet);
  };

  return (
    <>
      <Box sx={{ maxWidth: 350, minWidth: 120 }}>
        <FormControl fullWidth>
          <InputLabel id="select-type-label">TYPE DE RECETTES</InputLabel>
          <Select
            labelId="select-type-label"
            id="select-type"
            value={type}
            label="Types de recettes ....."
            onChange={handleChange}
          >
            <MenuItem value="Entrée">Entrées</MenuItem>
            <MenuItem value="Plat">Plats</MenuItem>
            <MenuItem value="Dessert">Desserts</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </>
  );
}

export default SelectType;
