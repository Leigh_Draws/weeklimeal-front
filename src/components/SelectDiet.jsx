/* eslint-disable react/prop-types */
import { Box, FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useState } from "react";
import "../components/SelectDiet.css";

function SelectDiet({filterRecipes}) {
  const [diet, setDiet] = useState("");

  // Fonction au moment du change du select, change le state du "diet" par ce qui a été choisi puis lance la fonction filterRecipes qui filtre les recettes en fonction du "diet" choisi. En 2 temps 
  const handleChange = (event) => {
    const selectedDiet = event.target.value
    setDiet(selectedDiet);
    filterRecipes(selectedDiet)
  };

  return (
    <>
      <Box sx={{ maxWidth: 350, minWidth: 120 }}>
        <FormControl fullWidth>
          <InputLabel id="select-diet-label">RÉGIMES ALIMENTAIRES</InputLabel>
          <Select
            labelId="select-diet-label"
            id="select-diet"
            value={diet}
            label="Régimes Alimentaires ....."
            onChange={handleChange}
          >
            <MenuItem value="Végétarien">Végétarien</MenuItem>
            <MenuItem value="Viande">Avec de la viande</MenuItem>
            <MenuItem value="Végan">Vegan</MenuItem>
            <MenuItem value="Poisson">Avec du Poisson</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </>
  );
}

export default SelectDiet;
