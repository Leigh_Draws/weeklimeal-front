/* eslint-disable react/prop-types */
import "../components/Title.css"

function Title(props) {

    const { title } = props

    return(
        <>
        <h1 className="title">{ title }</h1>
        </>
    )
}

export default Title