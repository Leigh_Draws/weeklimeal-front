import { useState } from "react";
import Add from "../assets/icons/add.svg";
import Out from "../assets/icons/out.svg";
import "./AddButton.css";

function AddButton() {
  const [isAdded, setAdd] = useState(false);

  function toggleAdd() {
    setAdd(!isAdded);
  }

  // Modifier le bouton pour utiliser les icones de Material UI / Icons
  return (
    <>
        <input
          className="add-button"
          id="add-button"
          type="image"
          alt="ajouter aux favoris"
          src={isAdded ? Out : Add }
          onClick={toggleAdd}
        />
    </>
  );
}

export default AddButton;
