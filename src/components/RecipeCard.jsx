/* eslint-disable react/prop-types */
import AddButton from "../components/AddButton";
import CountryTag from "./CountryTag";
import "./RecipeCard.css";
import Tag from "./Tag";

function RecipeCard(props) {
  const { name, duration, tags, country, image } = props;

  return (
    <>
      <div className="recipe-card">
        <AddButton />
        <div className="recipe-card-info">
          <div className="recipe-title">
            <h3>{name}</h3>
            <p className="recipe-duration">{duration} min</p>
          </div>
          <div className="recipe-tags">
            { country ? <CountryTag country={country} /> : false }
            {tags.map((eachtag) => (
              <Tag key={eachtag} tags={eachtag} />
            ))}
          </div>
        </div>
        <div className="recipe-filter"></div>
        <img className="recipe-img" src={image} alt={name} />
      </div>
    </>
  );
}

export default RecipeCard;
