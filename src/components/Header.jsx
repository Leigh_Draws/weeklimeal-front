import Logo from "../assets/icons/logo.svg";
import User from "../assets/icons/profile.svg";
import { NavLink } from "react-router-dom";
import "./Header.css";

function Header() {
  return (
    <>
      <header className="header-section">
        <div className="header-logo">
          <a href="#">
            <img src={Logo} alt="Logo Weekli Meal" className="logo" />
          </a>
        </div>
        <div className="header-nav">
            <li>
              <NavLink to='/'>Recettes</NavLink>
            </li>
            <li>
              <NavLink to='/favoris'>Favorites</NavLink>
            </li>
            <li>
              <NavLink to='/menus' className="a-secondary">Menus</NavLink>
            </li>
            <li>
              <NavLink to='/lists' className="a-secondary">Listes</NavLink>
            </li>
          <div className="header-profile">
            <NavLink to='/profile' >
              <img src={User} alt="Profil" />
            </NavLink>
          </div>
        </div>
      </header>
    </>
  );
}

export default Header;
